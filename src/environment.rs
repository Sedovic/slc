use crate::builtins;
use crate::lang;
use crate::program::Value;

use std::collections::HashMap;
use std::rc::Rc;

#[derive(Clone, Debug, PartialEq)]
pub struct Environment {
    map: HashMap<String, Value>,
}

impl Environment {
    pub fn empty() -> Self {
        Self {
            map: HashMap::new(),
        }
    }

    pub fn lookup(&self, name: &str) -> Option<Value> {
        self.map.get(name).cloned()
    }

    pub fn set<S: Into<String>>(&mut self, name: S, value: Value) {
        self.map.insert(name.into(), value);
    }

    pub fn overridable_key(&self, name: &str) -> bool {
        !name.starts_with("lang/") &&
            PartialEq::ne(name, lang::DEFINE) &&
            PartialEq::ne(name, lang::DEFINE_SYNTAX) &&
            PartialEq::ne(name, lang::IMPORT) &&
            PartialEq::ne(name, lang::LAMBDA) &&
            // NOTE: this allows to add new items with slashes but not override existing ones
            // TODO: do we wanna keep it?
            (!name.contains('/') || !self.map.contains_key(name))
    }

    pub fn merge(&mut self, other: &Environment) {
        for (key, value) in other.map.iter() {
            if self.overridable_key(key) {
                self.set(key.clone(), value.clone());
            }
        }
    }
}

impl Default for Environment {
    fn default() -> Self {
        let mut env = Self::empty();
        // Standard Library
        env.set(builtins::PLUS2, Value::Builtin(Rc::new(builtins::Plus2)));
        env
    }
}
