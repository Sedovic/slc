use crate::context::Context;
use crate::error::Error;
use crate::lambda::Lambda;
use crate::lang;
use crate::lexer;
use crate::parser;
pub use crate::value::Value;

use std::fmt::Debug;
use std::rc::Rc;

pub trait Callable: Debug {
    fn call(&self, arguments: &[Value], context: &Context) -> Result<Value, Error>;
}

#[derive(Debug)]
pub enum Parameters {
    List(Vec<String>),
    CaptureAll(String),
}

pub fn evaluate(value: Value, context: &mut Context) -> Result<Value, Error> {
    match value {
        Value::Symbol(symbol) => match context.environment.lookup(&symbol) {
            Some(result) => Ok(result),
            None => Err(Error::UnknownSymbol(symbol)),
        },
        Value::List(list) => {
            // NOTE: Handle special forms before evaluating anything
            if let Some(Value::Symbol(value)) = list.first() {
                match value.as_str() {
                    lang::DEFINE => {
                        if context.environment.lookup(value).is_some() {
                            return lang::define(&list, context);
                        } else {
                            return Err(Error::UnknownSymbol(value.to_string()));
                        }
                    }
                    lang::DEFINE_SYNTAX => {
                        if context.environment.lookup(value).is_some() {
                            return lang::define_syntax(&list, &mut context.environment);
                        } else {
                            return Err(Error::UnknownSymbol(value.to_string()));
                        }
                    }
                    lang::IMPORT => return lang::import(&list, &mut context.environment),
                    lang::LAMBDA => {
                        if context.environment.lookup(value).is_some() {
                            return lang::lambda(&list, &context.environment);
                        } else {
                            return Err(Error::UnknownSymbol(value.to_string()));
                        }
                    }
                    lang::LOAD => {
                        if context.environment.lookup(value).is_some() {
                            return lang::load(&list, context);
                        } else {
                            return Err(Error::UnknownSymbol(value.to_string()));
                        }
                    }
                    _ => {}
                }
            }

            if list.is_empty() {
                return Err(Error::CallingEmptyList);
            }
            match evaluate(list[0].clone(), context)? {
                Value::Builtin(callable) => {
                    let list = list
                        .iter()
                        .skip(1)
                        .cloned()
                        .map(|term| evaluate(term, context))
                        .collect::<Result<Vec<_>, Error>>();
                    let list = list?;
                    callable.call(&list, context)
                }
                Value::Macro(lambda) => {
                    let list = list
                        .iter()
                        .skip(1)
                        .cloned()
                        .map(|term| {
                            let wrapped_param = Lambda {
                                environment: context.environment.clone(),
                                parameters: Parameters::List(vec![]),
                                body: term,
                            };
                            Value::Builtin(Rc::new(wrapped_param))
                        })
                        .collect::<Vec<Value>>();
                    lambda.call(&list, context)
                }
                value => Err(Error::CallingUnexpectedValue(value)),
            }
        }
        Value::String(..) | Value::Number(..) | Value::Macro(..) => Ok(value),
        Value::Builtin(_) => Ok(value),
    }
}

pub fn run_str(program: &str, context: &mut Context) -> Result<Option<Value>, Error> {
    run(program.chars(), context)
}

pub fn run(
    program: impl IntoIterator<Item = char>,
    context: &mut Context,
) -> Result<Option<Value>, Error> {
    let tokens = lexer::tokens(program);
    let terms = parser::parse(tokens);
    // TODO: process (compile) this further and build an AST with
    // actual function calls, macro expansion, etc.?

    let mut result = Ok(None);
    for term in terms {
        result = evaluate(term?.into(), context).map(Some);
        if result.is_err() {
            break;
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::environment::Environment;

    #[test]
    fn empty_program() {
        assert_eq!(run_str("", &mut Context::new()), Ok(None));
    }

    #[test]
    fn single_number() {
        assert_eq!(
            run_str("42", &mut Context::new()),
            Ok(Some(Value::Number(42)))
        );
    }

    #[test]
    fn multiple_numbers() {
        assert_eq!(
            run_str("10 15 42", &mut Context::new()),
            Ok(Some(Value::Number(42)))
        );
    }

    #[test]
    fn no_environment() {
        assert_eq!(
            run_str("42", &mut Context::from_environment(Environment::empty())),
            Ok(Some(Value::Number(42)))
        );
        assert_eq!(
            run_str(
                "(math/plus2 1 2)",
                &mut Context::from_environment(Environment::empty())
            ),
            Err(Error::UnknownSymbol("math/plus2".into()))
        );
    }

    #[test]
    fn custom_environment_set_constant() {
        let mut ctx = Context::from_environment(Environment::empty());
        ctx.environment.set("TEN", Value::Number(10));
        assert_eq!(run_str("TEN", &mut ctx), Ok(Some(Value::Number(10))));
    }

    #[test]
    fn custom_environment_no_prelude() {
        let mut ctx = Context::from_environment(Environment::empty());
        ctx.environment.set("TEN", Value::Number(10));
        assert_eq!(run_str("TEN", &mut ctx), Ok(Some(Value::Number(10))));
        assert_eq!(
            run_str("(math/plus2 1 2)", &mut ctx),
            Err(Error::UnknownSymbol("math/plus2".into()))
        );
    }

    #[test]
    fn custom_environment_native_function() {
        #[derive(Clone, Debug, PartialEq)]
        struct Square;

        impl Callable for Square {
            fn call(&self, arguments: &[Value], _: &Context) -> Result<Value, Error> {
                match arguments.first() {
                    Some(Value::Number(num)) => Ok(Value::Number(*num * *num)),
                    Some(_) => Err(Error::IncorrectType),
                    None => Err(Error::ArgumentCount(1, arguments.len())),
                }
            }
        }

        let mut ctx = Context::from_environment(Environment::empty());
        ctx.environment
            .set("square", Value::Builtin(Rc::new(Square)));
        assert_eq!(
            run_str("(square 10)", &mut ctx),
            Ok(Some(Value::Number(100)))
        );
        assert_eq!(
            run_str("(math/plus2 1 2)", &mut ctx),
            Err(Error::UnknownSymbol("math/plus2".into()))
        );
    }

    #[test]
    fn builtin_call() {
        assert_eq!(
            run_str("(math/plus2 7 8)", &mut Context::new()),
            Ok(Some(Value::Number(15)))
        );
    }

    #[test]
    fn extend_environment_with_constant() {
        let mut ctx = Context::new();
        ctx.environment.set("TEN", Value::Number(10));
        assert_eq!(run_str("TEN", &mut ctx), Ok(Some(Value::Number(10))));
        assert_eq!(
            run_str("(math/plus2 TEN 2)", &mut ctx),
            Ok(Some(Value::Number(12)))
        );
    }

    #[test]
    fn nested_call() {
        assert_eq!(
            run_str(
                "(math/plus2 (math/plus2 1 2) (math/plus2 3 (math/plus2 2 2)))",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(10)))
        );
    }

    #[test]
    fn constant_definition() {
        assert_eq!(
            run_str(
                "(import (scheme base))(define ANSWER 42) ANSWER",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(42)))
        );
    }

    #[test]
    fn function_definition() {
        assert_eq!(
            run_str(
                "(import (scheme base))(define plus-3 (lambda (a b c) (math/plus2 a (math/plus2 b c)))) (plus-3 1 2 3)",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(6)))
        );
    }

    #[test]
    fn reserved_keyword_shadowing_define() {
        assert_eq!(
            run_str(
                "(import (scheme base))(define define (math/plus2 1 3))",
                &mut Context::new()
            ),
            Err(Error::ShadowingReservedItem("define".into()))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))(define lambda (math/plus2 1 3))",
                &mut Context::new()
            ),
            Err(Error::ShadowingReservedItem("lambda".into()))
        );
    }

    #[test]
    fn reserved_keyword_shadowing_lambda() {
        assert_eq!(
            run_str(
                "(import (scheme base))(lambda (define) 1)",
                &mut Context::new()
            ),
            Err(Error::ShadowingReservedItem("define".into()))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))(lambda (lambda) 1)",
                &mut Context::new()
            ),
            Err(Error::ShadowingReservedItem("lambda".into()))
        );
    }

    #[test]
    fn name_shadowing() {
        assert_eq!(run_str("(import (scheme base))(define plus2 math/plus2) (define plus2 (lambda (a _b) (math/plus2 a 20))) (plus2 1 2)", &mut Context::new()),
                   Ok(Some(Value::Number(21))));
        assert_eq!(run_str("(import (scheme base))(define plus2 math/plus2) (define add-ten (lambda (plus2 b) (math/plus2 plus2 b))) (add-ten 1 2)", &mut Context::new()),
        Ok(Some(Value::Number(3))));
    }

    #[test]
    fn immediate_lambda_execution() {
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda () (math/plus2 3 5)))",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(8)))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda (a) (math/plus2 a 5)) 3)",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(8)))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda (a b) (math/plus2 a b)) 3 5)",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(8)))
        );
    }

    #[test]
    fn closure() {
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda (a) ((lambda (b) (math/plus2 a b)) 5)) 3)",
                &mut Context::new()
            ),
            Ok(Some(Value::Number(8)))
        );
    }

    #[test]
    fn variadic_lambda() {
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda args args))",
                &mut Context::new()
            ),
            Ok(Some(Value::List(vec![])))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda args args) 1)",
                &mut Context::new()
            ),
            Ok(Some(Value::List(vec![Value::Number(1)])))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda args args) 1 2)",
                &mut Context::new()
            ),
            Ok(Some(Value::List(vec![Value::Number(1), Value::Number(2)])))
        );
        assert_eq!(
            run_str(
                "(import (scheme base))((lambda args args) 1 2 3)",
                &mut Context::new()
            ),
            Ok(Some(Value::List(vec![
                Value::Number(1),
                Value::Number(2),
                Value::Number(3),
            ])))
        );
    }
}
