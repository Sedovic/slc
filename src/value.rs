use crate::lambda::Lambda;
use crate::parser::Term;
use crate::program::Callable;

use std::fmt::{self, Display, Formatter};
use std::rc::Rc;

#[derive(Debug)]
pub enum Value {
    Number(i128),
    String(String),
    Symbol(String),
    List(Vec<Value>),

    // TODO: Can we remove the Rc now that we impl all the traits
    // manually?
    //
    // NOTE: looks like we can't because `Callable` is not `Clone` so
    // we can't create a new box out of it.
    //
    // TODO: rename this from `Builtin` to `Callable` or add a new `Lambda` variant. Because
    // Lambdas are callable values as well.
    Builtin(Rc<dyn Callable>),

    // NOTE: Macros are just lambdas with a different evaluation rules.
    Macro(Rc<Lambda>),
}

impl Value {
    pub fn number(number: i128) -> Self {
        Value::Number(number)
    }

    pub fn string(string: impl Into<String>) -> Self {
        Value::String(string.into())
    }

    pub fn symbol(name: impl Into<String>) -> Self {
        Value::Symbol(name.into())
    }

    pub fn empty() -> Self {
        Value::List(vec![])
    }
}

impl Clone for Value {
    fn clone(&self) -> Self {
        match self {
            Value::Builtin(callable) => Value::Builtin(callable.clone()),
            Value::Number(number) => Value::Number(*number),
            Value::String(string) => Value::String(string.clone()),
            Value::Symbol(symbol) => Value::Symbol(symbol.clone()),
            Value::List(list) => Value::List(list.clone()),
            Value::Macro(lambda) => Value::Macro(lambda.clone()),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Value) -> bool {
        match (self, other) {
            (Value::Builtin(_a), Value::Builtin(_b)) => unimplemented!(),
            (Value::Macro(_a), Value::Macro(_b)) => unimplemented!(),
            (Value::Number(a), Value::Number(b)) => a == b,
            (Value::String(a), Value::String(b)) => a == b,
            (Value::Symbol(a), Value::Symbol(b)) => a == b,
            (Value::List(a), Value::List(b)) => a == b,
            _ => false,
        }
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        use self::Value::*;
        match self {
            Number(num) => write!(f, "{}", num),
            String(string) => write!(f, "{:?}", string),
            Symbol(symbol) => write!(f, "{}", symbol),
            List(terms) => {
                let formatted_terms = terms
                    .iter()
                    .map(|term| format!("{}", term))
                    .collect::<Vec<_>>()
                    .join(" ");
                write!(f, "({})", formatted_terms)
            }
            Builtin(builtin) => write!(f, "[builtin {:?}]", builtin),
            Macro(lambda) => write!(f, "[macro {}]", lambda),
        }
    }
}

impl From<Term> for Value {
    fn from(term: Term) -> Self {
        match term {
            Term::Number(number) => Value::Number(number),
            Term::String(string) => Value::String(string),
            Term::Symbol(symbol) => Value::Symbol(symbol),
            Term::List(list) => {
                Value::List(list.into_iter().map(|term| term.into()).collect::<Vec<_>>())
            }
        }
    }
}
