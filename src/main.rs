use slc::context;
use slc::environment;
use slc::error;
use slc::program;

use std::env;
use std::fmt::{self, Debug, Formatter};
use std::fs;
use std::io;

#[derive(Debug)]
enum Error {
    SlcError(error::Error),
    IoError(io::Error),
}

struct ProgramError {
    path: String,
    error: Error,
}

impl Debug for ProgramError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "Program '{}' failed to run. Reason: ", self.path)?;
        match self.error {
            Error::SlcError(ref error) => write!(f, "{:?}", error),
            Error::IoError(ref error) => {
                use std::io::ErrorKind::*;
                match error.kind() {
                    NotFound => write!(f, "file not found."),
                    PermissionDenied => write!(f, "permission denied."),
                    _ => write!(f, "{:?}", error),
                }
            }
        }
    }
}

fn main() -> Result<(), ProgramError> {
    // NOTE: the first arg is the name of the program so we skip it
    let args = env::args().skip(1).collect::<Vec<_>>();

    if args.is_empty() {
        unimplemented!("REPL");
    }

    let mut context = context::Context::from_environment(environment::Environment::default());
    let mut output = None;
    for path in args {
        let path = &path;
        let current_dir = ::std::env::current_dir().unwrap();
        context.program_path = Some(current_dir.join(path));
        let source = fs::read_to_string(path).map_err(|e| ProgramError {
            path: path.to_string(),
            error: Error::IoError(e),
        })?;
        output = program::run(source.chars(), &mut context).map_err(|e| ProgramError {
            path: path.to_string(),
            error: Error::SlcError(e),
        })?;
    }

    if let Some(output) = output {
        println!("-> {}", output)
    }
    Ok(())
}
