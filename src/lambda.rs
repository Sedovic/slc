use crate::context::Context;
use crate::environment::Environment;
use crate::error::Error;
use crate::program::{self, Callable, Parameters, Value};

use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub struct Lambda {
    pub(crate) environment: Environment,
    pub(crate) parameters: Parameters,
    pub(crate) body: Value,
}

impl Callable for Lambda {
    fn call(&self, arguments: &[Value], context: &Context) -> Result<Value, Error> {
        if let Parameters::List(ref parameters) = self.parameters {
            if parameters.len() != arguments.len() {
                return Err(Error::ArgumentCount(parameters.len(), arguments.len()));
            }
        }
        let mut ctx = context.clone();
        ctx.environment.merge(&self.environment);
        match self.parameters {
            Parameters::List(ref parameters) => {
                for (param, arg) in parameters.iter().zip(arguments.iter()) {
                    let param = param.to_string();
                    if ctx.environment.overridable_key(&param) {
                        ctx.environment.set(param, arg.clone());
                    } else {
                        // NOTE: This shouldn't ever happen because it was handled at the lambda
                        // definition. Still, keeping it here for now.
                        return Err(Error::ShadowingNamespacedItem(param));
                    }
                }
            }
            Parameters::CaptureAll(ref binding) => {
                if ctx.environment.overridable_key(binding) {
                    ctx.environment
                        .set(binding.clone(), Value::List(arguments.to_vec()));
                } else {
                    return Err(Error::ShadowingNamespacedItem(binding.clone()));
                }
            }
        }
        program::evaluate(self.body.clone(), &mut ctx)
    }
}

impl Display for Lambda {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        match self.parameters {
            Parameters::List(ref parameters) => {
                write!(f, "(lambda ({}) {:?})", parameters.join(" "), self.body)
            }
            Parameters::CaptureAll(ref binding) => {
                write!(f, "(lambda {} {:?})", binding, self.body)
            }
        }
    }
}
