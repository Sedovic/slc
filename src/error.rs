use crate::program;

#[derive(Debug, PartialEq)]
pub enum Error {
    // Lexer
    NumberUnexpectedCharacter(char),
    NumberTooLarge,
    UnclosedString,
    SymbolUnexpectedCharacter(char),

    // Parser
    MissingOpenParen,
    MissingCloseParen,

    // Program
    UnknownSymbol(String),
    ExpectedSymbol(String, program::Value),
    ExpectedString(program::Value),
    // TODO: `ShadowingNamespacedItem` is no longer used anywhere.
    // Do we want to remove it?
    ShadowingNamespacedItem(String),
    ShadowingReservedItem(String),
    CallingEmptyList,
    CallingUnexpectedValue(program::Value),
    LambdaSyntax,
    MacroSyntax,
    ParameterMustBeSymbol,
    IncorrectType,
    ArgumentCount(usize, usize),
    NumberOverflow,
    UnknownImportPath(Vec<program::Value>),
    ImportPathMustBeList(program::Value),
    Io(String),
    UnknownError(String),
}
