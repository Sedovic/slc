use crate::context::Context;
use crate::error::Error;
use crate::program::{Callable, Value};

pub const PLUS2: &str = "math/plus2";

#[derive(Clone, Debug, PartialEq)]
pub struct Plus2;

impl Callable for Plus2 {
    fn call(&self, arguments: &[Value], _: &Context) -> Result<Value, Error> {
        match arguments {
            [Value::Number(a), Value::Number(b)] => match a.checked_add(*b) {
                Some(result) => Ok(Value::Number(result)),
                None => Err(Error::NumberOverflow),
            },
            [_, _] => Err(Error::IncorrectType),
            _ => Err(Error::ArgumentCount(2, arguments.len())),
        }
    }
}
