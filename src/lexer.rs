use crate::error::Error;

use std::iter::Peekable;

#[derive(Debug, PartialEq)]
pub enum Token {
    // TODO: add bignum?
    Number(i128),
    String(String),
    Symbol(String),
    OpenParen,
    ClosedParen,
}

pub struct Tokens<I>
where
    I: Iterator,
{
    glyphs: Peekable<I>,
    stopped: bool,
}

pub fn tokens(source: impl IntoIterator<Item = char>) -> Tokens<impl Iterator<Item = char>> {
    Tokens {
        glyphs: source.into_iter().peekable(),
        stopped: false,
    }
}

impl<I> Iterator for Tokens<I>
where
    I: Iterator<Item = char>,
{
    type Item = Result<Token, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.stopped {
            return None;
        }

        // Ignore any leading whitespace or comments
        loop {
            match self.glyphs.peek().cloned() {
                Some(';') => {
                    eat_comments(&mut self.glyphs);
                }
                Some(c) if c.is_ascii_whitespace() => {
                    eat_leading_whitespace(&mut self.glyphs);
                }
                _ => break,
            }
        }

        match self.glyphs.peek() {
            Some(_) => {
                let token = process_single_token(&mut self.glyphs);
                if token.is_err() {
                    self.stopped = true;
                }
                Some(token)
            }
            None => None,
        }
    }
}

/// Consume all leading whitespace from the iterator. After this
/// function finishes, calling `next` on the iterator will either
/// return a non-whitespace character or `None` because the iterator
/// is empty.
fn eat_leading_whitespace(glyphs: &mut Peekable<impl Iterator<Item = char>>) {
    loop {
        if let Some(glyph) = glyphs.peek().cloned() {
            if glyph.is_ascii_whitespace() {
                glyphs.next();
                continue;
            }
        }
        break;
    }
}

/// Consume a line comment. I.e. the semicolon character `;` and
/// anything following it until the new line.
fn eat_comments(glyphs: &mut Peekable<impl Iterator<Item = char>>) {
    if let Some(';') = glyphs.peek() {
        loop {
            match glyphs.peek() {
                Some('\n') => {
                    glyphs.next();
                    break;
                }
                None => break,
                Some(_) => {
                    glyphs.next();
                    continue;
                }
            }
        }
    }
}

/// Reads a single Token from the input stream. Returns error on
/// malformed or unexpected input.
///
/// Panics on empty iterator.
fn process_single_token(glyphs: &mut Peekable<impl Iterator<Item = char>>) -> Result<Token, Error> {
    let first_glyph = glyphs
        .peek()
        .cloned()
        .expect("The glyphs iterator must not be empty!");
    match first_glyph {
        '(' => {
            glyphs.next();
            Ok(Token::OpenParen)
        }

        ')' => {
            glyphs.next();
            Ok(Token::ClosedParen)
        }

        '+' | '-' | '0'..='9' => {
            let sign = match first_glyph {
                '+' => {
                    glyphs.next();
                    1
                }
                '-' => {
                    glyphs.next();
                    -1
                }
                _ => 1,
            };
            let mut number = 0i128;

            while let Some(next) = glyphs.peek() {
                if next.is_ascii_whitespace() || is_special_token(*next) {
                    break;
                }
                match glyphs.peek().cloned() {
                    None => break,
                    Some(next) if next.is_ascii_whitespace() => break,
                    Some(next) if is_special_token(next) => break,
                    Some(_) => {}
                }

                if let Some(glyph) = glyphs.next() {
                    let digit = glyph.to_digit(10);
                    match digit {
                        Some(digit) => {
                            match number.checked_mul(10) {
                                Some(multiplied) => {
                                    number = multiplied;
                                }
                                None => {
                                    return Err(Error::NumberTooLarge);
                                }
                            }
                            match number.checked_add(digit as i128 * sign) {
                                Some(added) => {
                                    number = added;
                                }
                                None => {
                                    return Err(Error::NumberTooLarge);
                                }
                            }
                        }
                        None => return Err(Error::NumberUnexpectedCharacter(glyph)),
                    }
                }
            }
            Ok(Token::Number(number))
        }

        '"' => {
            let mut string = String::new();
            glyphs.next();
            let mut closed_quote = false;
            for glyph in glyphs {
                if glyph == '"' {
                    closed_quote = true;
                    break;
                } else {
                    string.push(glyph);
                }
            }

            if closed_quote {
                Ok(Token::String(string))
            } else {
                Err(Error::UnclosedString)
            }
        }

        _ => {
            if !valid_symbol_character(first_glyph) {
                return Err(Error::SymbolUnexpectedCharacter(first_glyph));
            }
            let mut symbol = String::new();
            loop {
                match glyphs.peek() {
                    None => break,
                    Some(next) if next.is_ascii_whitespace() => break,
                    Some(next) if is_special_token(*next) => break,
                    Some(_) => {}
                }
                if let Some(glyph) = glyphs.next() {
                    if glyph.is_ascii_whitespace() {
                        break;
                    }
                    if valid_symbol_character(glyph) {
                        symbol.push(glyph);
                    } else {
                        return Err(Error::SymbolUnexpectedCharacter(glyph));
                    }
                }
            }
            Ok(Token::Symbol(symbol))
        }
    }
}

/// Returns true if the given character can be a part of a Symbol.
fn valid_symbol_character(glyph: char) -> bool {
    match glyph {
        '/' | '-' | '_' | '?' | '!' | '=' | '&' | '*' | ':' | '>' | '<' => true,
        glyph => glyph.is_ascii_alphanumeric(),
    }
}

fn is_special_token(glyph: char) -> bool {
    glyph == '(' || glyph == ')'
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_input() {
        assert_eq!(tokens("".chars()).collect::<Vec<_>>(), vec![])
    }

    #[test]
    fn standalone_number() {
        assert_eq!(
            tokens("0".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(0))]
        );
        assert_eq!(
            tokens("42".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(42))]
        );
        assert_eq!(
            tokens("-15".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(-15))]
        );
        assert_eq!(
            tokens("+15".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(15))]
        );
        // We support i128:
        assert_eq!(
            tokens("98347598437598374598439857984387534958".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(98347598437598374598439857984387534958))]
        );
        assert_eq!(
            tokens("-93480943250932093245023405994329349853".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(-93480943250932093245023405994329349853))]
        );
    }

    #[test]
    fn standalone_string() {
        assert_eq!(
            tokens(r#""""#.chars()).collect::<Vec<_>>(),
            vec![Ok(Token::String("".into()))]
        );
        assert_eq!(
            tokens(r#""hello""#.chars()).collect::<Vec<_>>(),
            vec![Ok(Token::String("hello".into()))]
        );
        assert_eq!(
            tokens(r#""Hello, World!""#.chars()).collect::<Vec<_>>(),
            vec![Ok(Token::String("Hello, World!".into()))]
        );
        assert_eq!(
            tokens(r#""42""#.chars()).collect::<Vec<_>>(),
            vec![Ok(Token::String("42".into()))]
        );
    }

    #[test]
    fn standalone_symbol() {
        assert_eq!(
            tokens("hello".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("hello".into()))]
        );
        assert_eq!(
            tokens("r".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("r".into()))]
        );
        assert_eq!(
            tokens("m33".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("m33".into()))]
        );
        assert_eq!(
            tokens("hello-world".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("hello-world".into()))]
        );
        assert_eq!(
            tokens("hello_world".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("hello_world".into()))]
        );
        assert_eq!(
            tokens("try!".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("try!".into()))]
        );
        assert_eq!(
            tokens("even?".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Symbol("even?".into()))]
        );
    }

    #[test]
    fn standalone_parens() {
        assert_eq!(
            tokens("(".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::OpenParen)]
        );
        assert_eq!(
            tokens(")".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::ClosedParen)]
        );
    }

    #[test]
    fn number_parse_character() {
        assert_eq!(
            tokens("42a".chars()).collect::<Vec<_>>(),
            vec![Err(Error::NumberUnexpectedCharacter('a'))]
        );
    }

    #[test]
    fn number_overflow() {
        assert_eq!(
            tokens("983475984375983745984398579843875349583984579437".chars()).collect::<Vec<_>>(),
            vec![Err(Error::NumberTooLarge)]
        );
        assert_eq!(
            tokens("-934809432509320932450234059943293498530".chars()).collect::<Vec<_>>(),
            vec![Err(Error::NumberTooLarge)]
        );
    }

    #[test]
    fn unclosed_string() {
        assert_eq!(
            tokens(r#"""#.chars()).collect::<Vec<_>>(),
            vec![Err(Error::UnclosedString)]
        );
        assert_eq!(
            tokens(r#""hello"#.chars()).collect::<Vec<_>>(),
            vec![Err(Error::UnclosedString)]
        );
    }

    #[test]
    fn multiple_tokens() {
        use self::Token::*;
        assert_eq!(
            tokens(r#"24 times write! "LOL" flush-buffer"#.chars()).collect::<Vec<_>>(),
            vec![
                Ok(Number(24)),
                Ok(Symbol("times".into())),
                Ok(Symbol("write!".into())),
                Ok(String("LOL".into())),
                Ok(Symbol("flush-buffer".into())),
            ]
        );

        assert_eq!(
            tokens(" 1\n2  \n\n\n3    ".chars()).collect::<Vec<_>>(),
            vec![Ok(Number(1)), Ok(Number(2)), Ok(Number(3))]
        );
    }

    #[test]
    fn lists() {
        use self::Token::*;
        assert_eq!(
            tokens("()".chars()).collect::<Vec<_>>(),
            vec![Ok(OpenParen), Ok(ClosedParen)]
        );
        assert_eq!(
            tokens("(1 2 3)".chars()).collect::<Vec<_>>(),
            vec![
                Ok(OpenParen),
                Ok(Number(1)),
                Ok(Number(2)),
                Ok(Number(3)),
                Ok(ClosedParen),
            ]
        );
        assert_eq!(
            tokens("(add 1 2 (mul 3 4))".chars()).collect::<Vec<_>>(),
            vec![
                Ok(OpenParen),
                Ok(Symbol("add".into())),
                Ok(Number(1)),
                Ok(Number(2)),
                Ok(OpenParen),
                Ok(Symbol("mul".into())),
                Ok(Number(3)),
                Ok(Number(4)),
                Ok(ClosedParen),
                Ok(ClosedParen),
            ]
        );
        assert_eq!(
            tokens("(print-newline)".chars()).collect::<Vec<_>>(),
            vec![
                Ok(OpenParen),
                Ok(Symbol("print-newline".into())),
                Ok(ClosedParen),
            ]
        );
        assert_eq!(
            tokens(r#"(print "hello")"#.chars()).collect::<Vec<_>>(),
            vec![
                Ok(OpenParen),
                Ok(Symbol("print".into())),
                Ok(String("hello".into())),
                Ok(ClosedParen),
            ]
        );
    }

    #[test]
    fn comments_in_empty_program() {
        assert_eq!(
            tokens(";this is a comment".chars()).collect::<Vec<_>>(),
            vec![]
        );
        assert_eq!(
            tokens(";this is a comment\n\n\n".chars()).collect::<Vec<_>>(),
            vec![]
        );
        assert_eq!(
            tokens("\n   \t  \n\t  ;this is a comment".chars()).collect::<Vec<_>>(),
            vec![]
        );
        assert_eq!(
            tokens("\n \t  ;this is a comment\n;; another comment".chars()).collect::<Vec<_>>(),
            vec![]
        );
    }

    #[test]
    fn comment_out_code() {
        assert_eq!(
            tokens("42\n;(def nope)\nlol  ;; whatever".chars()).collect::<Vec<_>>(),
            vec![Ok(Token::Number(42)), Ok(Token::Symbol("lol".into()))]
        );
    }
}
