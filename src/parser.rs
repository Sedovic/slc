use crate::error::Error;
use crate::lexer::Token;

use std::fmt::{self, Debug, Formatter};
use std::iter::Peekable;

#[derive(Clone, PartialEq)]
pub enum Term {
    Number(i128),
    String(String),
    Symbol(String),
    List(Vec<Term>),
}

impl Debug for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        use self::Term::*;
        match self {
            Number(num) => write!(f, "{:?}", num),
            String(string) => write!(f, "{:?}", string),
            Symbol(symbol) => write!(f, "{}", symbol),
            List(terms) => {
                let formatted_terms = terms
                    .iter()
                    .map(|term| format!("{:?}", term))
                    .collect::<Vec<_>>()
                    .join(" ");
                write!(f, "({})", formatted_terms)
            }
        }
    }
}

impl From<Token> for Term {
    fn from(token: Token) -> Self {
        match token {
            Token::Number(num) => Term::Number(num),
            Token::String(string) => Term::String(string.clone()),
            Token::Symbol(symbol) => Term::Symbol(symbol.clone()),
            Token::OpenParen => panic!("Cannot convert standalone OpenParen into a Term."),
            Token::ClosedParen => panic!("Cannot convert standalone ClosedParen into a Term."),
        }
    }
}

pub struct Terms<I>
where
    I: Iterator<Item = Result<Token, Error>>,
{
    iter: Peekable<I>,
    stopped: bool,
}

impl<I> Iterator for Terms<I>
where
    I: Iterator<Item = Result<Token, Error>>,
{
    type Item = Result<Term, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.iter.peek().is_none() || self.stopped {
            return None;
        }
        let term = parse_term(&mut self.iter);
        if term.is_err() {
            self.stopped = true;
        }
        Some(term)
    }
}

pub fn parse(
    tokens: impl IntoIterator<Item = Result<Token, Error>>,
) -> impl Iterator<Item = Result<Term, Error>> {
    Terms {
        iter: tokens.into_iter().peekable(),
        stopped: false,
    }
}

fn parse_term(tokens: &mut dyn Iterator<Item = Result<Token, Error>>) -> Result<Term, Error> {
    let token = tokens.next().unwrap();
    let term = match token? {
        Token::OpenParen => parse_list(tokens)?,
        Token::ClosedParen => return Err(Error::MissingOpenParen),
        token => token.into(),
    };
    Ok(term)
}

fn parse_list(tokens: &mut dyn Iterator<Item = Result<Token, Error>>) -> Result<Term, Error> {
    let mut result = vec![];
    loop {
        let term = match tokens.next() {
            Some(Ok(Token::ClosedParen)) => break,
            // NOTE: this ties our max list nesting depth to the
            // Rust's maximum recursion limit. We may want to use our
            // own stack or something here.
            Some(Ok(Token::OpenParen)) => parse_list(tokens)?,
            Some(Ok(token)) => token.into(),
            Some(Err(lexer_error)) => return Err(lexer_error),
            None => return Err(Error::MissingCloseParen),
        };
        result.push(term);
    }
    Ok(Term::List(result))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_program() {
        assert_eq!(parse(vec![]).collect::<Vec<_>>(), vec![]);
    }

    #[test]
    fn atom() {
        assert_eq!(
            parse(vec![Ok(Token::Number(42))]).collect::<Vec<_>>(),
            vec![Ok(Term::Number(42))]
        );
        assert_eq!(
            parse(vec![Ok(Token::String("Hello, World!".into()))]).collect::<Vec<_>>(),
            vec![Ok(Term::String("Hello, World!".into()))]
        );
        assert_eq!(
            parse(vec![Ok(Token::Symbol("run!".into()))]).collect::<Vec<_>>(),
            vec![Ok(Term::Symbol("run!".into()))]
        );
    }

    #[test]
    fn multiple_atoms() {
        assert_eq!(
            parse(vec![
                Ok(Token::Number(23)),
                Ok(Token::String("Hello, World!".into())),
                Ok(Token::Symbol("run!".into())),
            ]).collect::<Vec<_>>(),
            vec![
                Ok(Term::Number(23)),
                Ok(Term::String("Hello, World!".into())),
                Ok(Term::Symbol("run!".into())),
            ]
        );
    }

    #[test]
    fn empty_list() {
        assert_eq!(
            parse(vec![Ok(Token::OpenParen), Ok(Token::ClosedParen)]).collect::<Vec<_>>(),
            vec![Ok(Term::List(vec![]))]
        );
    }

    #[test]
    fn list_of_numbers() {
        assert_eq!(
            parse(vec![
                Ok(Token::OpenParen),
                Ok(Token::Number(1)),
                Ok(Token::Number(2)),
                Ok(Token::Number(3)),
                Ok(Token::ClosedParen),
            ]).collect::<Vec<_>>(),
            vec![Ok(Term::List(vec![
                Term::Number(1),
                Term::Number(2),
                Term::Number(3),
            ]))]
        );
    }

    #[test]
    fn list_of_everything() {
        assert_eq!(
            parse(vec![
                Ok(Token::OpenParen),
                Ok(Token::Number(42)),
                Ok(Token::String("Hello, World!".into())),
                Ok(Token::Symbol("run!".into())),
                Ok(Token::ClosedParen),
            ]).collect::<Vec<_>>(),
            vec![Ok(Term::List(vec![
                Term::Number(42),
                Term::String("Hello, World!".into()),
                Term::Symbol("run!".into()),
            ]))]
        );
    }

    #[test]
    fn nested_list() {
        // (fn hello () (echo! "Hello, World") 42)
        assert_eq!(
            parse(
                vec![
                    Token::OpenParen,
                    Token::Symbol("fn".into()),
                    Token::Symbol("hello".into()),
                    Token::OpenParen,
                    Token::ClosedParen,
                    Token::OpenParen,
                    Token::Symbol("echo!".into()),
                    Token::String("Hello, World!".into()),
                    Token::ClosedParen,
                    Token::Number(42),
                    Token::ClosedParen,
                ].into_iter()
                .map(Ok)
            ).collect::<Vec<_>>(),
            vec![Ok(Term::List(vec![
                Term::Symbol("fn".into()),
                Term::Symbol("hello".into()),
                Term::List(vec![]),
                Term::List(vec![
                    Term::Symbol("echo!".into()),
                    Term::String("Hello, World!".into()),
                ]),
                Term::Number(42),
            ]))]
        );
    }

    #[test]
    fn unbalanced_parentheses() {
        assert_eq!(
            parse(vec![Ok(Token::OpenParen), Ok(Token::Number(1))]).collect::<Vec<_>>(),
            vec![Err(Error::MissingCloseParen)]
        );
        assert_eq!(
            parse(vec![Ok(Token::ClosedParen), Ok(Token::Number(1))]).collect::<Vec<_>>(),
            vec![Err(Error::MissingOpenParen)]
        );
    }
}
