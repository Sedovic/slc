use crate::context::Context;
use crate::environment::Environment;
use crate::error::Error;
use crate::lambda::Lambda;
use crate::lexer;
use crate::parser;
use crate::program::{self, Parameters, Value};

use std::fs;
use std::rc::Rc;

pub const DEFINE: &str = "define";
pub const DEFINE_SYNTAX: &str = "define-syntax";
pub const IMPORT: &str = "import";
pub const LAMBDA: &str = "lambda";

pub const BASE: &str = "base";
pub const LOAD: &str = "load";
pub const SCHEME: &str = "scheme";
pub const WRITE: &str = "write";

pub fn define(list: &[Value], context: &mut Context) -> Result<Value, Error> {
    match (list.get(1), list.get(2)) {
        (Some(Value::Symbol(name)), Some(definition)) => {
            if context.environment.overridable_key(name) {
                let value = definition.clone();
                let value = program::evaluate(value, context)?;
                context.environment.set(name.to_string(), value);
                Ok(Value::Symbol(name.to_string()))
            } else {
                Err(Error::ShadowingReservedItem(name.clone()))
            }
        }
        _ => Err(Error::ArgumentCount(2, list.len())),
    }
}

pub fn define_syntax(list: &[Value], environment: &mut Environment) -> Result<Value, Error> {
    match (list.get(1), list.get(2)) {
        (Some(Value::Symbol(name)), Some(Value::List(lambda))) => {
            if environment.overridable_key(name) {
                let value = Value::Macro(Rc::new(build_lambda(lambda, environment)?));
                environment.set(name.to_string(), value.clone());
                Ok(value)
            } else {
                Err(Error::ShadowingReservedItem(name.clone()))
            }
        }
        (Some(Value::Symbol(..)), Some(..)) => Err(Error::MacroSyntax),
        _ => Err(Error::ArgumentCount(2, list.len())),
    }
}

pub fn import(list: &[Value], environment: &mut Environment) -> Result<Value, Error> {
    match list.first() {
        Some(Value::Symbol(symbol)) if symbol == IMPORT => {
            for import in &list[1..] {
                import_into_environment(import, environment)?;
            }
            Ok(Value::empty())
        }
        Some(value) => Err(Error::ExpectedSymbol(IMPORT.to_string(), value.clone())),
        None => Err(Error::ArgumentCount(2, list.len())),
    }
}

fn import_into_environment(list: &Value, environment: &mut Environment) -> Result<(), Error> {
    match list {
        Value::List(path) => {
            if path == &[Value::symbol(SCHEME), Value::symbol(BASE)] {
                // TODO: we're using markers just to show whether the symbols were
                // actually loaded. The actual handling of the special forms is
                // still done in `program::evaluate`.
                environment.set(DEFINE, Value::empty());
                environment.set(LAMBDA, Value::empty());
                environment.set(DEFINE_SYNTAX, Value::empty());
                Ok(())
            } else if path == &[Value::symbol(SCHEME), Value::symbol(LOAD)] {
                environment.set(LOAD, Value::empty());
                Ok(())
            } else if path == &[Value::symbol(SCHEME), Value::symbol(WRITE)] {
                //environment.set(WRITE, Value::empty());
                // TODO: this is just so the examples syntax check. We're not doing
                // anything with (scheme write) yet.
                Ok(())
            } else {
                Err(Error::UnknownImportPath(path.clone()))
            }
        }
        value => Err(Error::ImportPathMustBeList(value.clone())),
    }
}

pub(crate) fn lambda(list: &[Value], environment: &Environment) -> Result<Value, Error> {
    // TODO: check for lambda syntax errors
    // TODO: process argument list and map it to environment or something?
    Ok(Value::Builtin(Rc::new(build_lambda(list, environment)?)))
}

pub(crate) fn load(list: &[Value], context: &mut Context) -> Result<Value, Error> {
    use std::path::Path;
    if list.len() != 2 {
        return Err(Error::ArgumentCount(2, list.len()));
    }

    match list.get(1) {
        Some(Value::String(loaded_file_path)) => {
            // TODO: report an error the caller can process here.
            let path = context
                .program_path
                .clone()
                .map(|p| {
                    if p.is_file() {
                        p.parent()
                            .map(|p| p.join(loaded_file_path))
                            .unwrap_or(Path::new(loaded_file_path).into())
                    } else if p.is_dir() {
                        p.join(loaded_file_path)
                    } else {
                        panic!("Path '{:?}' is neither a file nor directory!", p.display());
                    }
                }).unwrap_or(Path::new(loaded_file_path).into());
            let source = fs::read_to_string(path).map_err(|e| Error::Io(format!("{}", e)))?;
            let tokens = lexer::tokens(source.chars());
            let terms = parser::parse(tokens);
            for term in terms {
                program::evaluate(term?.into(), context)?;
            }
            Ok(Value::empty())
        }
        Some(val) => Err(Error::ExpectedString(val.clone())),
        None => Err(Error::ArgumentCount(2, list.len())),
    }
}

fn build_lambda(list: &[Value], environment: &Environment) -> Result<Lambda, Error> {
    match (list.get(1), list.get(2)) {
        (Some(Value::List(parameters)), Some(body)) => {
            let parameters = parameters
                .iter()
                .map(|t| match t {
                    Value::Symbol(s) => {
                        let s = s.to_string();
                        if environment.overridable_key(&s) {
                            Ok(s)
                        } else {
                            Err(Error::ShadowingReservedItem(s))
                        }
                    }
                    _ => Err(Error::ParameterMustBeSymbol),
                }).collect::<Result<Vec<String>, Error>>();
            Ok(Lambda {
                environment: environment.clone(),
                parameters: Parameters::List(parameters?),
                body: body.clone(),
            })
        }
        (Some(Value::Symbol(args_binding)), Some(body)) => Ok(Lambda {
            environment: environment.clone(),
            parameters: Parameters::CaptureAll(args_binding.clone()),
            body: body.clone(),
        }),
        // TODO: check the actual syntax issue and report it here
        _ => Err(Error::LambdaSyntax),
    }
}
