use crate::environment::Environment;

use std::path::PathBuf;

#[derive(Clone)]
pub struct Context {
    pub environment: Environment,
    pub program_path: Option<PathBuf>,
}

impl Default for Context {
    fn default() -> Self {
        Self::new()
    }
}

impl Context {
    pub fn new() -> Self {
        Self::from_environment(Environment::default())
    }

    pub fn from_environment(environment: Environment) -> Self {
        Context {
            environment,
            program_path: None,
        }
    }
}
