slc (Something Lisp Cool)
=========================

This is like a lispy language I wrote in Rust on a train ride to my
folks.


Building Static Binary
======================

Using musl, we should be able to produce a binary that runs on any linux:

    $ rustup target add x86_64-unknown-linux-musl --toolchain=stable
    $ cargo build --release --target x86_64-unknown-linux-musl
    $ ls target/x86_64-unknown-linux-musl/release/slc

If the above fails because of some system dependencies, e.g. if we ever add `curl`,
we should be able to use cross:

    $ cargo install cross
    $ cross build --target=x86_64-unknown-linux-musl
