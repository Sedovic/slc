all:
	cargo check
	cargo build
	cargo build --release
	cargo build --release --target x86_64-unknown-linux-musl
	cargo test
	cargo test --target x86_64-unknown-linux-musl


.PHONY: all
