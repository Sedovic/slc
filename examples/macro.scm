(import (scheme base))

;; This is a version of `if' that's not handling the conditions
;; specially, which means it evaluates everything when called.
(define immediate-if (lambda (condition then-body else-body)
                       ((condition then-body) else-body)))

;; And this is the macro version that does postpone the evaluation
;; only after the condition has been resolved.
;;
;; NOTE: The way we do macros is: define syntax is just like `define',
;; but it signals the interpreter that the evaluation should be
;; different. Upon "calling" the form (`if') defined here, the
;; interpreter will wrap all the argumens in lambdas, delaying their
;; evaluation. The macro body here must evaluate whatever it needs
;; explicitly by "calling" the parameter (which leads to evaluating
;; the lambda).
(define-syntax if (lambda (condition then-body else-body)
                    ((immediate-if (condition) then-body else-body))))

;; Infinite loop. This is the only externally observable thing we can
;; do now.
(define loop (lambda () (loop)))


;; Verify `if' really doesn't evaluate both arguments. If it did, we
;; would loop indefinitely (or stack overflow since we don't do TCO
;; yet).
;;
;; If the macros work, `loop' will never be called and we will receive
;; the `finished' string.
(if false
    (loop)
    "finished")
