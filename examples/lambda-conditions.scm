(import (scheme base))

(define true (lambda (first second) first))
(define false (lambda (first second) second))
(define if (lambda (condition then-body else-body)
             (condition then-body else-body)))

(if false 1 2)
