(import (scheme base))

((lambda (true false if)
         (if false 1 2))

 (lambda (first second) first)
 (lambda (first second) second)
 (lambda (condition then-body else-body)
   (condition then-body else-body)))
