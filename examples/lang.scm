(import (scheme base))

(define list (lambda arg arg))

(define-syntax quote (lambda (val) val))

;; Return two results: a nested list and a quoted list
(list
 (quote lol)
 (list 1 2 3 (list 4 5) (list 6))
 (quote (1 2 3)))
