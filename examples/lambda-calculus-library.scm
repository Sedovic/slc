(import (scheme base))

(define true (lambda (x) (lambda (y) x)))
(define false (lambda (x) (lambda (y) y)))

;; NOTE: this evaluates both bodies! You must pass them as lambdas
;; and evaluate the final block if you want the expected `if' behaviour.
(define if (lambda (condition then-body else-body)
             ((condition then-body) else-body)))


(define and-nested (lambda (p) (lambda (q) ((p q) p))))
;; convenience so we can write `(and a b)' instead of `((and a) b)'
(define and (lambda (a b) ((and-nested a) b)))

(define identity (lambda (n) n))

(define successor
  (lambda (n)
    (lambda (f)
      (lambda (x)
        (f ((n f) x))))))

(define predecessor
  (lambda (n)
    (lambda (f)
      (lambda (x)
        (((n (lambda (g) (lambda (h) (h (g f))))) (lambda (u) x)) (lambda (u) u))))))

(define subtract
  (lambda (m n)
    (lambda (f)
      (lambda (x)
        ((((n predecessor) m) f) x)))))

(define zero  (lambda (f) (lambda (x) x)))
(define one   (lambda (f) (lambda (x) (f x))))
(define two   (lambda (f) (lambda (x) (f (f x)))))
(define three (lambda (f) (lambda (x) (f (f (f x))))))
(define four  (successor three))
(define five  (successor four))
(define six   (successor five))
(define seven (successor six))
(define eight (successor seven))
(define nine  (successor eight))
(define ten   (successor nine))

(define zero? (lambda (n) ((n (lambda (x) false)) true)))
(define less-or-equal? (lambda (m n) (zero? (subtract m n))))
(define equal? (lambda (a b) (and (less-or-equal? a b) (less-or-equal? b a))))
