(import (scheme base))

;; Convenience functions for turning a Church numeral into a Number
(define int-inc (lambda (n) (math/plus2 n 1)))
(define church->int (lambda (n) ((n int-inc) 0)))


(if (less-or-equal? zero one)
    (lambda () "true")
    (lambda () "false"))

(if (and true true)
    (lambda () "true")
    (lambda () "false"))

(if (and true false)
    (lambda () "true")
    (lambda () "false"))

(if (equal? (successor two) (predecessor four))
    (lambda () "true")
    (lambda () "false"))

(church->int nine)
