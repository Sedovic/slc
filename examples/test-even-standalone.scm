(import (scheme load) (scheme write))
(load "lambda-calculus-library.scm")

(define minus-two (lambda (num) (predecessor (predecessor num))))

;; NOTE: we need to use lambdas in the bodies and then evaluate them
;; immediately here. This is because the `if' form does evaluates both
;; blocks when being called.
(define even? (lambda (num)
                (((if (equal? num zero)
                    (lambda () (lambda () "true"))
                    (lambda () (if (equal? num one)
                                   (lambda () "false")
                                   (lambda () (even? (minus-two num))))))))))

(even? four)
;; TODO: uncomment this once we support `display' and `newline'
;; (display (even? four))
;; (newline)
;; (display "lol")
