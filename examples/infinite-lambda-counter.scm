(import (scheme base))

(define recursion
          (lambda (n)
            (recursion (math/plus2 1 n))))

(recursion 0)
