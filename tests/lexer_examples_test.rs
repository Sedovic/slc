use slc;

use std::fs;

#[test]
fn examples_have_no_errors() {
    for entry in fs::read_dir("examples").unwrap() {
        let entry = entry.unwrap();
        let program = fs::read_to_string(entry.path())
            .expect(&format!("Failed to read file {:?}", entry.path()));
        let tokens = slc::lexer::tokens(program.chars());

        assert_eq!(
            tokens.filter(|tok| tok.is_err()).collect::<Vec<_>>(),
            vec![],
            "Error lexing file {:?}",
            entry.path(),
        );
    }
}
