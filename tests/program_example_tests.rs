use slc::context::Context;
use slc::program::{self, Value};

use std::fs;

#[test]
fn run_church_arithmetic() {
    let mut context = Context::new();
    let library = fs::read_to_string("examples/lambda-calculus-library.scm").unwrap();
    let program = fs::read_to_string("examples/church-arithmetic.scm").unwrap();

    program::run(library.chars(), &mut context).expect("Failed to load library.");
    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::Number(9)));
}

#[test]
fn run_lamda_based_bindings() {
    let mut context = Context::new();
    let library = fs::read_to_string("examples/lambda-calculus-library.scm").unwrap();
    let program = fs::read_to_string("examples/only-lambda.scm").unwrap();

    program::run(library.chars(), &mut context).expect("Failed to load library.");
    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::Number(2)));
}

#[test]
fn run_even_number_test() {
    let mut context = Context::new();
    let library = fs::read_to_string("examples/lambda-calculus-library.scm").unwrap();
    let program = fs::read_to_string("examples/test-even.scm").unwrap();

    program::run(library.chars(), &mut context).expect("Failed to load library.");
    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::string("true")));
}

#[test]
fn run_lambda_conditions() {
    let mut context = Context::new();
    let program = fs::read_to_string("examples/lambda-conditions.scm").unwrap();

    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::Number(2)));
}

#[test]
fn run_if_true_example() {
    let mut context = Context::new();
    let library = fs::read_to_string("examples/lambda-calculus-library.scm").unwrap();
    let program = fs::read_to_string("examples/if-true-example.scm").unwrap();

    program::run(library.chars(), &mut context).expect("Failed to load library.");
    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::string("true")));
}

#[test]
fn run_if_false_example() {
    let mut context = Context::new();
    let library = fs::read_to_string("examples/lambda-calculus-library.scm").unwrap();
    let program = fs::read_to_string("examples/if-false-example.scm").unwrap();

    program::run(library.chars(), &mut context).expect("Failed to load library.");
    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::string("false")));
}

#[test]
fn test_macro_example() {
    let mut context = Context::new();
    let library = fs::read_to_string("examples/lambda-calculus-library.scm").unwrap();
    let program = fs::read_to_string("examples/macro.scm").unwrap();

    program::run(library.chars(), &mut context).expect("Failed to load library.");
    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::string("finished")));
}

#[test]
fn test_even_example() {
    let mut context = Context::new();
    let current_dir = ::std::env::current_dir().unwrap();
    context.program_path = Some(current_dir.join("examples/test-even-standalone.scm"));
    let program = fs::read_to_string("examples/test-even-standalone.scm").unwrap();

    let result = program::run(program.chars(), &mut context).expect("Failed to run the program.");

    assert_eq!(result, Some(Value::string("true")));
}
