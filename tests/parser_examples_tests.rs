use slc;

use std::fs;

#[test]
fn examples_have_no_parse_errors() {
    for entry in fs::read_dir("examples").unwrap() {
        let entry = entry.unwrap();
        let program = fs::read_to_string(entry.path())
            .expect(&format!("Failed to read file {:?}", entry.path()));
        let tokens = slc::lexer::tokens(program.chars());
        let terms = slc::parser::parse(tokens);

        assert_eq!(
            terms.filter(|term| term.is_err()).collect::<Vec<_>>(),
            vec![],
            "Error parsing file {:?}",
            entry.path(),
        );
    }
}
