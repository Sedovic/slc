Grammar
-------

    Sexp  = List | Atom .
    List  = "(" { Whitespace } ")" | "(" Sexp { Whitespace Sexp } ")" .
    Atom  = Number | String | Symbol .

    Number     = Digit { Digit } .
    String     = `"` Character { Character } `"` .
    Symbol     = Character { Character } .

    Digit      = "0" ... "9" .
    Character  = /* any Unicode character excluding whitespace and parens */ .
    Whitespace = /* any Unicode whitespace character */ .

The characters will probably need a proper specification.

The Unicode spec: <http://www.unicode.org/versions/Unicode6.0.0/>

Special Forms
-------------

### Lambda ###

    (lambda (Symbol1 Symbol2 ... SymbolN) (Sexp))

`lambda` takes exactly two arguments: arglist and an expression.

Arglist is a list of 0 or more symbols. When the lambda is called, these
symbols will be bound to the provided values.

Expression may contain the symbols specified in the `arglist`. Calling lambda
will cause the expression to be evaluated and its result will be returned by
the lambda.

Note that `lambda` does not support multiple expressions in its body. To
evaluate multiple expressions in a lambda's body, one must wrap them in `prog`.


#### Named functions ####

Lambda can be bound to a symbol just like any other expression:

    (def inc (lambda (n) (+ n 1)))

This is a quite common use case so there is a shortcut provided by the `defn`
macro:

    (defn inc (n) (+ n 1))

The two examples are identical. Upon evaluation, `defn` will expand the latter
code to the form shown in the first example.

#### Recursion ####

The symbol `self` Inside the `lambda` body is provided to refer to the `lambda`
itself. This allows calling the `lambda` recursively even when it's not bound
to any name.

Example:

    (lambda (n)
            (if (= n 0)
                1
                (* n (self (dec n)))))

This anonymous function computes factorial zero or a positive integer.

#### Variable arity ####

A `lambda` may be instructed to accept a variable number of arguments.

This is done separating the last symbol in the `lambda`'s signature with
_ampersand_ character (`&`).

    (def foo (lambda (one two & rest)
        rest))

All the symbols before the ampersand separator will have the passed values
bound as usual. The last symbol will receive a list containing all the
remaining values or `nil` if there are none:

    ==> (foo 1 2)
    ; nil
    ==> (foo 1 2 3)
    ; (3)
    ==> (foo 1 2 3 4)
    ; (3 4)

#### Closures ####

If a `lambda` is declared inside another `lambda`, the inner one will "inherit"
the local bindings of the outer one and these will persist even outside their
original scope:

    (defn adder (x)
      (lambda (y) (+ x y)))
    ;=> adder
    ((adder 5) 3)
    ;=> 8

As you can see, the binding of the number `5` to the symbol `x` is available in
the inner `lambda` even after the `added` function has returned.


### Quote ###

`quote` takes one argument and returns it without evaluating. When no arguments
are passed, the result is `nil`.

    ==> (quote)
    ; nil
    ==> (quote 1)
    ; 1
    ==> (quote hello)
    ; hello
    ==> (quote (1 2 3 4))
    ; (1 2 3 4)
    ==> (quote 1 2 3 4)
    ; 1


### Prog ###

    (prog
      Sexp1
      Sexp2
      ...
      SexpN)

`prog` takes zero or more expressions as its arguments. It evaluets the
expressions in the specified order. `prog` returns the result of the last
evaluated expression.

If there are no expressions in `prog`'s body, `nil` is returned.

If an expression results in an error, expressions that follows it will not be
evaluated and `prog` will return `nil`.


### Def ###

    (def name Sexp)

`def` takes exactly two arguments: a symbol and an expression.

The expression will be bound to the symbol in the `global context`.

If the symbol is already bound, it will be rebound to the new value.

Evaluating `def` returns `nil`.


### If ###

    (if condition SexpTrue SexpFalse)

`if` takes exactly three arguments: a condition and two expressions.

`condition` will be evaluated. If the result is either `nil` or `false`, `if`
will evaluate `SexpFalse` and return its result.

Otherwise, `if` will evaluate `SexpTrue` and return its result.

If there is a need to run more than one expression in the evaluated body,
`prog` must be used.


### Car ###

    (car (list 1 2 3 4))
    ;=> 1
    (car ())
    ;=> nil

`car` returns the first item of the given list. If the list is empty, `car`
will return `nil`.


### Cdr ###

    (cdr (list 1 2 3 4))
    ;=> (2 3 4)
    (cdr (1))
    ;=> ()
    (cdr ())
    ;=> ()

`cdr` returns a list of all values but the first one. `cdr` of a list with one
or zero items returns an empty list.


### Macro ###

    (def unless (macro cond falsey truthy)
      (list (quote if) cond truthy falsey))
    ;=> unless
    (def x 5)
    ;=> x
    (unless (= x 0) (divide 10 x) nil)
    ;=> 2
    (def x 0)
    (unless (= x 0) (divide 10 x) nil)
    ;=> nil

`macro` has the same structure as `lambda`.  Unlike `lambda` however, it does
not evaluate its arguments upon passing. The arguments are passed as
s-expressions bound to the symbols in `macros`'s signature.

When the `macro` is called, it evaluates its body and returns an s-expression.
Upon return, the s-expression is evaluated.

Macros are a powerful tool for extending the core language. Unlike lambdas,
there is almost never a need to use an anonymous macro. Macros are usually
using `defmacro`:

    (defmacro unless (cond falsey truthy)
      (list (quote if) cond truthy falsey))



TODO
----

* Parallel lexing/parsing/evaluating using goroutines
* Add support for strings
* REPL
* I/O capabilities (reading from keyboard, writing to screen, reading/writing files)
